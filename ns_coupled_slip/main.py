__author__ = 'maks'

"""
This is an example of usage Navier-Stokes equations with Slip boundary conditions.

If I use both iterative solver and Slip bc with detailed mesh it throw an error due to non convergence.

IF I use direct or LU solver and Slip bc with detailed mesh it solves the problem correctly and without errors.

For enable/disable "Slip bc" use the following command:
parameters_dict_["is_slip"] = True/False

For enable/disable "iterative solver" use the following command:
parameters_dict_["is_iterative_solver"] = True

If you define "parameters_dict_["is_slip"] = False", a solver will use "NoSlip bc" for velocity.
"""

# from dolfin import *

import dolfin

import ns_coupled


# Set dolfin log level
dolfin.set_log_level(dolfin.ERROR)
if dolfin.MPI.rank(dolfin.mpi_comm_world()) == 0:
    # set_log_level(PROGRESS)
    # dolfin.set_log_level(dolfin.DEBUG)
    dolfin.set_log_level(dolfin.INFO)


if __name__ == "__main__":
    """
    It calls Navier-Stokes Coupled solver.
    It solver a rectangle geometry with inlet/outlet/wall boundary conditions.

    Inlet bc:
        u_inlet = constant

    Outlet bc:
        p_outlet = constant

    Wall bc:
        For NoSlip bc:
            u_wall = 0

        For Slip bc:
            u_wall * normal = 0
            d(u_tangential)/d(normal) = 0


    More precisely it firstly defines all required parameters and
    then calls "ns_coupled.solve_(parameters_dict=parameters_dict_)" for solving a problem.
    """

    # Define a force

    # def define_force(mesh):
    def define_force(dimension):
        force = dolfin.Constant((0.0,) * dimension)
        # force = dolfin.Constant((0.0,) * mesh.topology().dim())

        # force = dolfin.Constant((0.0, 0.0))
        # force = dolfin.Constant((-1.0, 0.0))
        # force = dolfin.Constant((10.0, 0.0))
        # force = dolfin.Constant((-10.0, 0.0))
        # force = dolfin.Constant((1.0, 0.0))

        if dimension == 3:
            pass
            # force = dolfin.Constant((0.0, 0.0, -1.0))
            # force = dolfin.Constant((0.0, 0.0, -10.0))

        return force

    # Define a mesh

    def define_mesh(is_problem_3d):

        if not is_problem_3d:
            # mesh = dolfin.RectangleMesh(0.0, 0.0, 10.0, 1.0, 1, 1, "crossed")
            # mesh = dolfin.RectangleMesh(0.0, 0.0, 10.0, 1.0, 2, 2, "crossed")

            # mesh = dolfin.RectangleMesh(0.0, 0.0, 10.0, 1.0, 10, 5, "crossed")

            # mesh = dolfin.RectangleMesh(0.0, 0.0, 10.0, 1.0, 20, 5, "crossed")
            mesh = dolfin.RectangleMesh(0.0, 0.0, 10.0, 1.0, 20, 10, "crossed")
            # mesh = dolfin.RectangleMesh(0.0, 0.0, 10.0, 1.0, 40, 20, "crossed")

            # Best mesh
            # mesh = dolfin.RectangleMesh(0.0, 0.0, 10.0, 1.0, 80, 40, "crossed")
            # Best mesh

            # mesh = dolfin.RectangleMesh(0.0, 0.0, 10.0, 1.0, 160, 80, "crossed")
            # mesh = dolfin.RectangleMesh(0.0, 0.0, 10.0, 1.0, 320, 160, "crossed")

        else:
            # BoxMesh(x0, y0, z0, x1, y1, z1, nx, ny, nz)

            mesh = dolfin.BoxMesh(0.0, 0.0, 0.0, 1.0, 1.0, 10.0, 4, 4, 8)

            # mesh = dolfin.BoxMesh(0.0, 0.0, 0.0, 1.0, 1.0, 10.0, 10, 10, 20)
            # mesh = dolfin.BoxMesh(0.0, 0.0, 0.0, 1.0, 1.0, 10.0, 10, 10, 40)
            # mesh = dolfin.BoxMesh(0.0, 0.0, 0.0, 1.0, 1.0, 10.0, 10, 10, 80)
            # mesh = dolfin.BoxMesh(0.0, 0.0, 0.0, 1.0, 1.0, 10.0, 20, 20, 40)
            # mesh = dolfin.BoxMesh(0.0, 0.0, 0.0, 1.0, 1.0, 10.0, 20, 20, 80)
            # mesh = dolfin.BoxMesh(0.0, 0.0, 0.0, 1.0, 1.0, 10.0, 40, 40, 80)

            # mesh = dolfin.BoxMesh(0.0, 0.0, 0.0, 1.0, 1.0, 10.0, 10, 10, 100)

        return mesh



    parameters_dict_ = {}

    #################################################################################
    # Define parameters.
    # Short description of an each parameter below.

    # is_slip: bool: use Slip / NoSlip boundary condition for velocity
    # is_iterative_solver: bool: use / not use iterative solver
    # folder_name: string: folder where solution will be saved
    # is_newton: bool: use / not use newton method for solving NS equation
    # is_transient: bool: transient or stationary solver
    # time: float: start time
    # time_end: float: end time
    # delta_t: float: time step

    # nu: float: kinematic viscosity
    # p_inlet: float: inlet pressure; not used in the current problem
    # p_outlet: float: outlet pressure
    # u_inlet: float: inlet velocity
    # iter_: int: number of a start iteration
    # max_iter: int: maximum number of iterations
    # tol: float: tolerance
    # eps: float: don't change

    # omega: float: relaxation parameter; if omega = 1.0 it means without relaxation
    # function_1.vector().axpy(-1, function_0.vector())
    # function_0.vector().axpy(omega, function_1.vector())

    # An example of a case without relaxation below or omega = 1.0
    # function_0.assign(function_1)

    # is_stabilization: bool: use / not use stabilization

    # f: volume function
    #################################################################################

    # # Get location of the current directory
    # import os
    # print os.getcwd()

    # Define a dolfin parameters
    dolfin.parameters["linear_algebra_backend"] = "PETSc"
    # dolfin.parameters["linear_algebra_backend"] = "uBLAS"

    dolfin.parameters["mesh_partitioner"] = "SCOTCH"
    dolfin.parameters["form_compiler"]["representation"] = "quadrature"
    dolfin.parameters["form_compiler"]["optimize"] = True
    dolfin.parameters["form_compiler"]["cpp_optimize"] = True
    dolfin.parameters["form_compiler"]["quadrature_degree"] = 4


    # Set a default parameters
    parameters_dict_.setdefault("is_problem_3d", False)
    parameters_dict_.setdefault("is_slip", False)
    parameters_dict_.setdefault("is_iterative_solver", False)
    parameters_dict_.setdefault("is_plot_solution", True)
    parameters_dict_.setdefault("is_save_solution", True)
    parameters_dict_.setdefault("folder_name", "results")
    parameters_dict_.setdefault("is_newton", False)
    parameters_dict_.setdefault("is_transient", True)
    parameters_dict_.setdefault("is_convection", True)
    parameters_dict_.setdefault("time", 0.0)
    parameters_dict_.setdefault("time_end", 10.0)
    parameters_dict_.setdefault("delta_t", 0.1)

    parameters_dict_.setdefault("linear_solver", "gmres")
    parameters_dict_.setdefault("preconditioner", "ilu")
    parameters_dict_.setdefault("nu", 0.1)
    parameters_dict_.setdefault("p_inlet", 11.5)
    parameters_dict_.setdefault("p_outlet", 0.0)
    parameters_dict_.setdefault("u_inlet", 1.0)
    parameters_dict_.setdefault("iter_", 0)
    parameters_dict_.setdefault("max_iter", 100)
    parameters_dict_.setdefault("tol", 1E-1)
    parameters_dict_.setdefault("eps", 1.0)
    parameters_dict_.setdefault("omega", 0.4)

    parameters_dict_.setdefault("is_stabilization", True)

    parameters_dict_["is_slip_test"] = True

    # Override some of the parameters below

    # parameters_dict_["nu"] = 0.01
    # parameters_dict_["nu"] = 0.001

    # parameters_dict_["max_iter"] = 1
    # parameters_dict_["max_iter"] = 2
    parameters_dict_["max_iter"] = 50

    parameters_dict_["time_end"] = 20.0
    parameters_dict_["delta_t"] = 0.01

    # parameters_dict_["omega"] = 0.8

    # parameters_dict_["tol"] = 1E-3
    parameters_dict_["tol"] = 1E-5

    # TEST
    parameters_dict_["p_outlet"] = 10.0
    # TEST

    parameters_dict_["is_slip"] = True
    parameters_dict_["is_transient"] = False
    parameters_dict_["is_stabilization"] = False
    parameters_dict_["is_iterative_solver"] = True
    # parameters_dict_["is_newton"] = True
    parameters_dict_["is_problem_3d"] = True

    parameters_dict_["linear_solver"] = "gmres"
    # parameters_dict_["linear_solver"] = "minres"

    # parameters_dict_["preconditioner"] = "amg"
    parameters_dict_["preconditioner"] = "ilu"
    # parameters_dict_["preconditioner"] = "petsc_amg"

    # parameters_dict_["is_plot_solution"] = False
    # parameters_dict_["is_save_solution"] = False

    # if not parameters_dict_["is_newton"]:
    #     parameters_dict_["omega"] = 1.0

    if not parameters_dict_["is_iterative_solver"]:
        parameters_dict_["linear_solver"] = "lu"
        # parameters_dict_["linear_solver"] = "mumps"

    if parameters_dict_["is_transient"] and not parameters_dict_["is_newton"]:
        parameters_dict_["omega"] = 1.0

    parameters_dict_["omega"] = 1.0

    mesh = define_mesh(parameters_dict_["is_problem_3d"])

    # parameters_dict_["mesh"] = define_mesh(parameters_dict_["is_problem_3d"])

    dimension = mesh.topology().dim()

    print "mesh dimensions = {}".format(dimension)

    f = define_force(dimension)

    parameters_dict_["f"] = f
    parameters_dict_["mesh"] = mesh
    parameters_dict_["dimension"] = dimension

    # dolfin.info(dolfin.memory_usage(True))

    ns_coupled.solve_(parameters_dict=parameters_dict_)

    # dolfin.info(dolfin.memory_usage(True))
