__author__ = 'maks'

"""
The module defines additional functions for plotting and saving solution.
"""

import dolfin


def plot_solution(function_, is_plot_solution):
    """
    Plot a function.

    Plot the "function_" if "is_plot_solution == True".

    :rtype : None
    :type function_: dolfin.functions.function.Function
    :type is_plot_solution: bool
    """

    if is_plot_solution:
        dolfin.plot(function_.split()[0], title="u", key="u", rescale=True)
        dolfin.plot(function_.split()[1], title="p", key="p", rescale=True)


def save_solution(function_, file_1, file_2, is_save_solution):
    """
    Save a function.

    Save the "function_" to the files "file_1, file_2" if "is_save_solution == True".

    :rtype : None
    :type function_: dolfin.functions.function.Function
    :type file_1: dolfin.cpp.io.File
    :type file_2: dolfin.cpp.io.File
    :type is_save_solution: bool
    """

    if is_save_solution:
        file_1 << function_.split()[0]
        file_2 << function_.split()[1]


def create_files(folder_name):
    """
    Create files for storing solution.

    Create files in the "folder_name" folder.

    :type folder_name: str
    :rtype :  dolfin.functions.function.Function
    :return: file_1, file_2
    """

    file_1 = dolfin.File(folder_name + "/" + "u.pvd")
    file_2 = dolfin.File(folder_name + "/" + "p.pvd")

    return file_1, file_2
